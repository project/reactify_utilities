<?php

namespace Drupal\reactify_client_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Client entities.
 *
 * @ingroup reactify_client_entity
 */
class ClientDeleteForm extends ContentEntityDeleteForm {


}
