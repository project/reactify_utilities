INTRODUCTION
------------

Reactify Theme Utilities module.

Provides necessary functionality and examples for Reactify theme integration.
Adds REST endpoints, authentication, CRUD endpoints for users, content and
comments.
Provides example of implementing custom entities, what would be suitable
for web applications.

Reactify theme is available at https://www.drupal.org/project/reactify.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

Module can be installed in usual way.
Module contains various submodules, so only needed functionality can be enabled.


CONFIGURATION
-------------

Module doesn't doesn't provide configuration interface.
It has optional configuration settings.


MAINTAINERS
-----------

Oleg Elizarov (olegel) - https://www.drupal.org/u/olegel
